# Webproxy

只需要在backend简单标记一个attribute或者decorator，生成接口信息，然后通过接口信息就能生成本地的proxy代码啦～

# Example

例如我们有一个C#写的一些webapi：

```c#
    class TestApiModule
    {
        [Webprox.Webproxy(ForMobile = true, UseValidate = true)]
        public void say()
        {
        }

        [Webprox.Webproxy(ForMobile = true, UseValidate = true)]
        public int say4(int a, string b)
        {
            return a + b.Length;
        }

        [Webprox.Webproxy(ForMobile = true, UseValidate = true)]
        public string[] say3(string a, string b)
        {
            return new string[] { a, b };
        }

        [Webprox.Webproxy(ForMobile = true, UseValidate = true)]
        public int say2(int[] a)
        {
            return a.Length;
        }

        [Webproxy(UseValidate=true)]
        public Dictionary<string,int> saysaysay()
        {
            Dictionary<string, int> dict = new Dictionary<string, int>();
            for (int i = 0; i < 1000; i++)
            {
                dict[i.ToString()] = i;
            }
            return dict;
        }
    }
```

先不管原来是怎样写，总之只需要在方法上面加上webproxy的属性～就能够parse出信息了

```javascript
{
  "TestApiModule": [
    {
      "MethodName": "say",
      "ReturnType": "",
      "Support": {
        "Desktop": 1,
        "Mobile": 1,
        "Web": 0
      },
      "IsValidate": true,
      "Arguments": {}
    },
    {
      "MethodName": "say4",
      "ReturnType": "i",
      "Support": {
        "Desktop": 1,
        "Mobile": 1,
        "Web": 0
      },
      "IsValidate": true,
      "Arguments": {
        "a": "i",
        "b": "s"
      }
    },
    {
      "MethodName": "say3",
      "ReturnType": "as",
      "Support": {
        "Desktop": 1,
        "Mobile": 1,
        "Web": 0
      },
      "IsValidate": true,
      "Arguments": {
        "a": "s",
        "b": "s"
      }
    },
    {
      "MethodName": "say2",
      "ReturnType": "i",
      "Support": {
        "Desktop": 1,
        "Mobile": 1,
        "Web": 0
      },
      "IsValidate": true,
      "Arguments": {
        "a": "ai"
      }
    },
    {
      "MethodName": "saysaysay",
      "ReturnType": "dsi",
      "Support": {
        "Desktop": 1,
        "Mobile": 0,
        "Web": 0
      },
      "IsValidate": true,
      "Arguments": {}
    }
  ]
}
```

然后frontend就可以根据这些元信息去生成接口去调用鸟～
