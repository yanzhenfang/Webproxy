﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Webprox;

namespace Webprox.Webdemo
{
    /// <summary>
    /// proxy 的摘要说明
    /// </summary>
    public class proxy : ProxyInvoker
    {

        public override string ModuleNamespace
        {
            get { return "Webprox.TestDemo"; }
        }

        public override bool Validate(string token, out int code ,out string msg)
        {
            code = 0x000;
            msg = "我就是说你验证不了啊";
            return true;
        }
    }
}