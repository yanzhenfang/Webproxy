﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Webprox;

namespace Webprox.Webdemo.API
{
    public class MyAPIS
    {
        [Webproxy]
        public void add()
        {
            throw new ProxyMethodInvokeException(0x101, "fuck you");
        }

        [Webproxy(ForDesktop=false, ForMobile=true)]
        public Dictionary<string, int> getTable()
        {
            Dictionary<string, int> dict = new Dictionary<string, int>();
            for (int i = 0; i < 1000; i++)
            {
                dict[i.ToString()] = i;
            }
            return dict;
        }
    }

    public class MyAPIS2
    {
        [Webproxy(UseValidate = true, CallInterval = 100)]
        public int plus(int a, int b)
        {
            return a + b;
        }
    }

    public class MyAPIS3
    {
        [Webproxy]
        public int minus(int a, int b)
        {
            if (b == 0)
                throw new ProxyMethodInvokeException(0x101, "yes sir");
            else return b - a;
        }
    }
}