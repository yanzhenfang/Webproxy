﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Webprox
{
    /// <summary>
    /// 调用proxy接口的异常
    /// </summary>
    public class ProxyMethodInvokeException:Exception
    {
        /// <summary>
        /// 构造异常
        /// </summary>
        /// <param name="code">接口调用的异常状态代码</param>
        /// <param name="msg">返回的信息</param>
        public ProxyMethodInvokeException(int code, string msg)
        {
            this.ProxyErrorCode = code;
            _proxyMessage = msg;
        }

        /// <summary>
        /// 接口调用的错误异常代码
        /// </summary>
        public int ProxyErrorCode { get; set; }
        /// <summary>
        /// 错误信息
        /// </summary>
        private string _proxyMessage = string.Empty;
        /// <summary>
        /// 重载获取message的方法
        /// </summary>
        public override string Message
        {
            get
            {
                return _proxyMessage;
            }
        }

    }
}
