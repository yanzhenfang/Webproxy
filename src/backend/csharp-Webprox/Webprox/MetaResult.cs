﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Newtonsoft.Json;

namespace Webprox
{
    /// <summary>
    /// 元数据结果返回类型，能够转换parse为Json，XML, plist， base64等
    /// </summary>
    public sealed class MetaResult
    {
        #region 元数据结果的属性

        /// <summary>
        /// 调用是否成功
        /// </summary>
        public bool Success { get; set; }
        /// <summary>
        /// 返回的code
        /// </summary>
        public int Code { get; set; }
        /// <summary>
        /// 返回的信息
        /// </summary>
        public string Message { get; set; }
        /// <summary>
        /// 返回的数据
        /// </summary>
        public object Data { get; set; }

        #endregion

        #region 其他的属性，不输出到Json

        /// <summary>
        /// 是否带格式输出
        /// </summary>
        [JsonIgnore]
        public bool IsPretty { get; set; }

        #endregion

        #region 转换为目标结果的格式

        /// <summary>
        /// 转换为Json的结果输出
        /// </summary>
        /// <returns></returns>
        public string Parse()
        {
            return this.Parse(IsPretty);
        }
        /// <summary>
        /// 转换为Json字符串输出，可以带格式
        /// </summary>
        /// <param name="isPretty">是否有更好的格式输出（缩进）</param>
        /// <returns></returns>
        public string Parse(bool isPretty)
        {
            if (isPretty)
                return JsonConvert.SerializeObject(this, Formatting.Indented);
            else
                return JsonConvert.SerializeObject(this);
        }

        #endregion
    }
}
