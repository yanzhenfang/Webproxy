﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Webprox
{
    /// <summary>
    /// 通过parser分析出来的接口模块缓存
    /// </summary>
    public class ProxyCache
    {
        /// <summary>
        /// 模块缓存存放
        /// </summary>
        private static Dictionary<string, object> _moduleCache = new Dictionary<string, object>();

        /// <summary>
        /// 设置缓存
        /// </summary>
        /// <param name="moduleName">模块名</param>
        /// <param name="module">模块对象</param>
        public static void Set(string moduleName, object module)
        {
            _moduleCache[moduleName] = module;
            GC.Collect();
        }

        /// <summary>
        /// 获取模块缓存
        /// </summary>
        /// <param name="moduleName">模块名</param>
        /// <returns></returns>
        public static object Get(string moduleName)
        {
            if (_moduleCache.ContainsKey(moduleName))
                return _moduleCache[moduleName];
            else
                return null;
        }

        /// <summary>
        /// 清空缓存
        /// </summary>
        public static void Clear()
        {
            _moduleCache.Clear();
            GC.Collect();
        }
        
    }
}
