﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Webprox
{
    /// <summary>
    /// Webproxy的Attribute，声明了这个Attribute的方法都会附带上属性被Parser识别，因此能生成接口的元信息
    /// </summary>
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = true, Inherited = false)]
    public sealed class WebproxyAttribute : Attribute
    {
        #region Proxy特性里面所具有的属性

        /// <summary>
        /// 是否支持桌面客户端调用
        /// </summary>
        public bool ForDesktop { get; set; }
        /// <summary>
        /// 是否支持手机客户端调用
        /// </summary>
        public bool ForMobile { get; set; }
        /// <summary>
        /// 是否支持Web调用(ajax/jsonp)
        /// </summary>
        public bool ForWeb { get; set; }
        /// <summary>
        /// 调用是否需要验证状态
        /// </summary>
        public bool UseValidate { get; set; }
        /// <summary>
        /// 接口的调用间隔，单位时间ms
        /// </summary>
        public uint CallInterval { get; set; }
        /// <summary>
        /// 移动端接口可以忽略输出的的字段，该字段是Data字段里面返回数据的字段
        /// </summary>
        public string[] IgnoreField { get; set; }

        #endregion

        #region 初始化proxy特性

        /// <summary>
        /// 初始化webproxy Attribute
        /// </summary>
        public WebproxyAttribute()
        {
            // 默认支持PC客户端类的调用
            ForDesktop = true;

            // 登录验证以及其他平台的支持默认不开启
            ForMobile = false;
            ForWeb = false;
            UseValidate = false;

            // 默认调用间隔为0，不限制
            CallInterval = 0;

            // 忽略字段设置，默认不设置
            IgnoreField = null;
        }

        #endregion

    }
}
