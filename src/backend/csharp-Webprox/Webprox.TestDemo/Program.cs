﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Reflection;

using Webprox;

namespace Webprox.TestDemo
{
    class Program
    {
        [Webprox.Webproxy(ForMobile = true, UseValidate = true)]
        public void say()
        {
        }

        [Webprox.Webproxy(ForMobile = true, UseValidate = true)]
        public int say4(int a, string b)
        {
            return a + b.Length;
        }

        [Webprox.Webproxy(ForMobile = true, UseValidate = true)]
        public string[] say3(string a, string b)
        {
            return new string[] { a, b };
        }

        [Webprox.Webproxy(ForMobile = true, UseValidate = true)]
        public int say2(int[] a)
        {
            return a.Length;
        }

        [Webproxy(UseValidate=true)]
        public Dictionary<string,int> saysaysay()
        {
            Dictionary<string, int> dict = new Dictionary<string, int>();
            for (int i = 0; i < 1000; i++)
            {
                dict[i.ToString()] = i;
            }
            return dict;
        }


        static void Main(string[] args)
        {
            // Parser Test
            //---------------------------------------------------------------
            MetaResult result = new MetaResult();
            result.Success = true;
            result.Code = 0;
            result.Message = "Hello world";
            result.Data = "Fuck you";
            Console.WriteLine("Json  :{0}\nPretty:{1}",
                result.Parse(),
                result.Parse()
                );

            // 遍历namespace
            //---------------------------------------------------------------            
            Webprox.ProxyParser parser = new ProxyParser("Webprox.TestDemo");
            Console.WriteLine(parser.ParseProxyMethods());
        }
    }
}
