# -*- coding:utf-8 -*-
'''
#=============================================================================
#     FileName: infoparser.py
#         Desc: 获取指定地址的元信息数据，然后parse为相应的代码
#       Author: drzunny
#        Email: drzunny@hotmail.com
#     HomePage: http://drzlab.info
#      Version: 0.1.0
#   LastChange: 2013-10-23 09:48:03
#      History:
#=============================================================================
'''
import httplib
import json

def getProxyMetaInfo(url):
    """获取proxy的元信息数据

    :url: 获取metainfo的地址 
    :returns: metainfo的信息

    """

    c = httplib.HTTPConnection(url)
    c.request("GET", '')
    res = c.getresponse()
    data = res.read()
    res.close()
    return json.loads(data)
