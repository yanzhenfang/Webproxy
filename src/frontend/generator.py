# -*- coding:utf-8 -*-
'''
#=============================================================================
#     FileName: generator.py
#         Desc: 生成器
#       Author: drzunny
#        Email: drzunny@hotmail.com
#     HomePage: http://drzlab.info
#      Version: 0.1.0
#   LastChange: 2013-10-23 09:58:30
#      History:
#=============================================================================
'''

def generate(platform, metainfo, output = './gen'):
    """ to Generate the target platform's web api proxy with metainfo

    :platform: 生成目标，和targets里面文件夹名匹配
    :metainfo: 用来生成数据的元信息
    :returns:  无返回

    """
    pass
